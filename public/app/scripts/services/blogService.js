angular.module('blogService', [])

    .factory('Blog', function($http) {
        return {
            get : function() {
                return $http.get('/api/v1/articles');
            }
        }
    });
