var blogApp =  angular.module('blogApp',['ngRoute','BlogControllers', 'blogService']); //'ngRoute', 'BlogControllers',

// create the routes for the angular build. Are called when http:// http://localhost:8000/app/index.html is called
blogApp.config(['$routeProvider',
    function($routeProvider) {
        $routeProvider.
        // all blog articles in list
        when('/blogs', {
            templateUrl: 'scripts/views/list.html',
            controller: 'blogController'
        }).
        // individual blog
        when('/blog/:id', {
            templateUrl: 'scripts/views/blog.html',
            controller: 'articleController'
        }).

        // auto forward to http://localhost:8000/app/index.html#/blogs
        otherwise({
            redirectTo: '/blogs'
        });
    }]);
