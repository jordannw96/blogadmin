angular.module('BlogControllers', [])

// there are two methods for retrieving data (there are actually more but these are the main two)

// method 1 - using a service (can make code more readable)
    .controller('blogController',//['$scope','$http', 'Blog',

        function($scope, $http, Blog){
            $scope.articles = {}; // create object to hold all of the data
            $scope.loading = true; // shows the spinning loading icon

            Blog.get()// use the service we previously created
                .success(function(data) {
                    $scope.articles = data.data; // load data to the object
                    $scope.loading = false; // turns off spinning icon
                });
        })


    // method 2 - call the data inline. (Sometimes easier if you want to use the variables in the uri)
    .controller('articleController', ['$scope','$routeParams', '$http',

        function($scope, $routeParams, $http){

            $http.get('/api/v1/articles/' + $routeParams.id).// use the routeParam.id to append the relevant article id to the uri of the api
            success(function(data) {
                $scope.article = data.data;  // load data to the object
                //  console.log($scope.article);
            });

        }]);
