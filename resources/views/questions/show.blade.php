@extends('layouts.app')

@section('content')
    <div class="col-md-8 col-md-offset-2">
        <div class="panel panel-default">
            <div class="panel-body">
                {{--the 3 lines of code below shows the questionnaire title and content and the questions user name, by
                pulling it from the database--}}
                <h1>{{ $questionnaire->title }}</h1>
                <p>{{ $questionnaire->content }}</p>
                <p>{{ $questions->user->name }}</p>

            </div>
        </div>
    </div>
@endsection













