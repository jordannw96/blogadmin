@extends('layouts.app')

@section('content')
    <div class="col-md-8 col-md-offset-2">
        <div class="panel panel-default">
            <div class="panel-body">
                {{--the two lines of code below display the title and content from the questionnaire, this is done by
                pulling through the questionnaire title and content from the database--}}
                <h1>{{ $questionnaire->title }}</h1>
                <p>{{ $questionnaire->content }}</p>
                {{--This is the start of the form, an array is openned which links the form to the Question controller, using the id and createQuestionnaire view --}}
                {!! Form::open(array('action' => 'QuestionController@store', 'id' => 'createQuestionnaire')) !!}
                {{ csrf_field() }}
                <br>
                {{-- for the following section of code, several forms are opened to create the questions for the questionnaires--}}
                {{--Question 1--}}
                <div class="row large-12 columns">
                    {!! Form::label('question_1', ' Question 1:  ') !!}
                    {!! Form::text('question_1', null, ['class' => 'large-8 columns']) !!}
                </div>

                List of answers:
                <div class="row large-12 columns">
                    {!! Form::label('answer_1', 'Option 1:') !!}
                    {!! Form::text('answer_1', null, ['class' => 'large-8 columns']) !!}
                </div>
                <div class="row large-12 columns">
                    {!! Form::label('answer_2', 'Option 2:') !!}
                    {!! Form::text('answer_2', null, ['class' => 'large-8 columns']) !!}
                </div>
                <div class="row large-12 columns">
                    {!! Form::label('answer_3', 'Option 3:') !!}
                    {!! Form::text('answer_3', null, ['class' => 'large-8 columns']) !!}
                </div>
                <br>
                {{-- for the following section of code, several forms are opened to create the questions for the questionnaires--}}
                {{--Question 2--}}

                <div class="row large-12 columns">
                    {!! Form::label('question_2', ' Question 2:  ') !!}
                    {!! Form::text('question_2', null, ['class' => 'large-8 columns']) !!}
                </div>
            List of answers:
            <div class="row large-12 columns">
                {!! Form::label('answer_4', 'Option 1:') !!}
                {!! Form::text('answer_4', null, ['class' => 'large-8 columns']) !!}
            </div>
            <div class="row large-12 columns">
                {!! Form::label('answer_5', 'Option 2:') !!}
                {!! Form::text('answer_5', null, ['class' => 'large-8 columns']) !!}
            </div>
            <div class="row large-12 columns">
                {!! Form::label('answer_6', 'Option 3:') !!}
                {!! Form::text('answer_6', null, ['class' => 'large-8 columns']) !!}
            </div>
                <br>
                {{-- for the following section of code, several forms are opened to create the questions for the questionnaires--}}
                {{--Question 3--}}

                <div class="row large-12 columns">
                    {!! Form::label('question_3', ' Question 3:  ') !!}
                    {!! Form::text('question_3', null, ['class' => 'large-8 columns']) !!}
                </div>
                List of answers:
                <div class="row large-12 columns">
                    {!! Form::label('answer_7', 'Option 1:') !!}
                    {!! Form::text('answer_7', null, ['class' => 'large-8 columns']) !!}
                </div>
                <div class="row large-12 columns">
                    {!! Form::label('answer_8', 'Option 2:') !!}
                    {!! Form::text('answer_8', null, ['class' => 'large-8 columns']) !!}
                </div>
                <div class="row large-12 columns">
                    {!! Form::label('answer_9', 'Option 3:') !!}
                    {!! Form::text('answer_9', null, ['class' => 'large-8 columns']) !!}
                </div>
                <br>
                {{-- for the following section of code, several forms are opened to create the questions for the questionnaires--}}
                {{--Question 4--}}

                <div class="row large-12 columns">
                    {!! Form::label('question_4', ' Question 4:  ') !!}
                    {!! Form::text('question_4', null, ['class' => 'large-8 columns']) !!}
                </div>
                List of answers:
                <div class="row large-12 columns">
                    {!! Form::label('answer_10', 'Option 1:') !!}
                    {!! Form::text('answer_10', null, ['class' => 'large-8 columns']) !!}
                </div>
                <div class="row large-12 columns">
                    {!! Form::label('answer_11', 'Option 2:') !!}
                    {!! Form::text('answer_11', null, ['class' => 'large-8 columns']) !!}
                </div>
                <div class="row large-12 columns">
                    {!! Form::label('answer_12', 'Option 3:') !!}
                    {!! Form::text('answer_12', null, ['class' => 'large-8 columns']) !!}
                </div>
                <br>
                {{-- for the following section of code, several forms are opened to create the questions for the questionnaires--}}
                {{--Question 5--}}

                <div class="row large-12 columns">
                    {!! Form::label('question_5', ' Question 5:  ') !!}
                    {!! Form::text('question_5', null, ['class' => 'large-8 columns']) !!}
                </div>
                List of answers:
                <div class="row large-12 columns">
                    {!! Form::label('answer_13', 'Option 1:') !!}
                    {!! Form::text('answer_13', null, ['class' => 'large-8 columns']) !!}
                </div>
                <div class="row large-12 columns">
                    {!! Form::label('answer_14', 'Option 2:') !!}
                    {!! Form::text('answer_14', null, ['class' => 'large-8 columns']) !!}
                </div>
                <div class="row large-12 columns">
                    {!! Form::label('answer_15', 'Option 3:') !!}
                    {!! Form::text('answer_15', null, ['class' => 'large-8 columns']) !!}
                </div>
                <br>
                <div class="row large-4 columns">
                    {!! Form::submit('Submit questions', ['class' => 'button']) !!}

                </div>

                <br>
                {{--here shows the creator of the questionnaire, this works by pulling the user name from the database--}}
                <p>Creator: {{ $questionnaire->user->name }}</p>
                {{--this hides the foreign key to the questionnaire table--}}
                {!! Form::hidden('questionnaire_id', $questionnaire->id, ['class' => 'large-8 columns']) !!}

                {!! Form::close() !!}




        </div>
        </div>
    </div>
@endsection










