@extends('layouts.app')

@section('content')
    <div class="col-md-8 col-md-offset-2">
        <div class="panel panel-default">
            <div class="panel-body">

    <title>Results</title>



<h1>Results</h1>

                {{--This is the start of the form, an array is openned which links the form to the Questionnaire controller,
                 using the id and createQuestionnaire view --}}
{!! Form::open(array('action' => 'QuestionnaireController@store', 'id' => 'createQuestionnaire')) !!}
{{ csrf_field() }}
                {{-- for the following section of code, several forms are opened to create the questions for the questionnaires--}}
<div class="row large-12 columns">
    {!! Form::label('title', ' Title:  ') !!}
    {!! Form::text('title', null, ['class' => 'large-8 columns']) !!}
</div>
                {{-- for the following section of code, several forms are opened to create the questions for the questionnaires--}}
<div class="row large-12 columns">
    {!! Form::label('content', 'Ethics:') !!}
    {!! Form::textarea('content', null, ['class' => 'large-8 columns']) !!}

</div>
                {{-- for the following section of code, several forms are opened to create the questions for the questionnaires--}}
<div class="row large-4 columns">
    {{--Here is a button which submits the form--}}
    {!! Form::submit('End Questionnaire', ['class' => 'button']) !!}
</div>
{!! Form::close() !!}

</div>
        </div>
    </div>
@endsection