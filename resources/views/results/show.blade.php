@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Questionnaire results </div>
                    <div class="panel-body">
                        {{--here it checks if the varible has a value, if it doesnt it wont display anything--}}
                        @if(isset($questionnaire))
                            {{--this line of code pulls the questionnaire id from the database--}}
                            <h1>Questionnaire:  {{ $questionnaire->id }}</h1>
                            <br>
                        @endif
                        {{--here it checks if the varible has a value, if it doesnt it wont display anything--}}
                        @if(isset($results))
                            {{--here a loop is created for showing the results to be displayed using @foreach,--}}
                            @foreach($results as $result)
                                <br>
                            {{--here the result id is pulled from the database--}}
                                <p>Respondent:{{$result->id}}</p>
                                <h3>Question 1</h3>
                            {{--the next block of code drags the answers 1-5 from the database and displays them under the h3 tags--}}
                               <p> {{ $result->answers_1}}</p>
                                    <h3>Question 2</h3>
                                    <p> {{ $result->answers_2}}</p>
                                    <h3>Question 3</h3>
                                    <p> {{ $result->answers_3}}</p>
                                    <h3>Question 4</h3>
                                    <p> {{ $result->answers_4}}</p>
                                    <h3>Question 5</h3>
                                    <p> {{ $result->answers_5}}</p>
                            @endforeach
                        @endif

                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection