<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Create Article</title>
<!-- <link rel="stylesheet" href="{{ asset('/css/app.css') }}" /> add in later tutorial for styling -->
</head>
<body>
<h1>Add Article</h1>

{!! Form::open(array('action' => 'ArticleController@store', 'id' => 'createarticle')) !!}
{{ csrf_field() }}
<div class="row large-12 columns">
    {!! Form::label('title', 'Title:') !!}
    {!! Form::text('title', null, ['class' => 'large-8 columns']) !!}
</div>

<div class="row large-12 columns">
    {!! Form::label('content', 'Detail:') !!}
    {!! Form::textarea('content', null, ['class' => 'large-8 columns']) !!}


<div class="row large-4 columns">
    {!! Form::submit('Add Article', ['class' => 'button']) !!}
</div>
    </div>
{!! Form::close() !!}


</body>
</html>
