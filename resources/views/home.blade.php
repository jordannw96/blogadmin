{{--here the layout is used, this is taken from the layouts folder which contains the layout for all views--}}
@extends('layouts.app')
{{--here a section is opened--}}
@section('content')
    <div class="col-md-8 col-md-offset-2">
        <div class="panel panel-default">
            <div class="panel-body">

        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Home Page</div>
                    <div class="panel-body">
                        {{--once the user has logged in it will display this message--}}
                        You are logged in!
                    </div>

                </div>
                {{--This is the start of the form, an array is openned which links the form to the Questionnaire controller--}}
                {{ Form::open(array('action' => 'QuestionnaireController@create', 'method' => 'get')) }}
                {{ Form::close() }}
                {{--this line of code is a link to create questionnaire view, when clicke will take the user there--}}
                <h1><a href="http://localhost:8000/admin/questionnaire/create"> Create a Questionnaire </a></h1>
            </div>
        </div>
                <br>
                <h1>Welcome to the online survey system</h1>
                <br>
                <h2>What the survey offers...</h2>
                <br>
                <li>The user is able to create a questionnaire with a title and ethics section</li>
                <li>The user is able to add multi-choice questions to the questionnaire created</li>
                <li>The user is able to to view the questionnaire with the multi-choice questions and answer them</li>
                <li>The user is able to answer the questionnaire</li>
                <li>The user is able to look at the results from all respondents</li>

            </div>
    </div>
    </div>

@endsection
<head>
    <style>
        .error {color: #FF0000;}
    </style>
</head>
<body>



