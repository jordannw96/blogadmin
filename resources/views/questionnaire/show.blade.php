@extends('layouts.app')
@section('content')
    <div class="col-md-8 col-md-offset-2">
        <div class="panel panel-default">
            <div class="panel-body">
                {{--the two lines of code below display the title and content from the questionnaire, this is done by
                pulling through the questionnaire title and content from the database--}}
                <h1>{{ $questionnaire->title }}</h1>
                <p>{{ $questionnaire->content }}</p>
                {{--here a loop is created for showing the questions to be displayed using @foreach, a from is then opened,
                 an array is created which links the form to the results controller using the id and results view--}}
                @foreach($question as $questions)
                {!! Form::open(array('action' =>'ResultsController@store', 'id' => 'results')) !!}
                    {{--Question 1--}}
                    <h3>Question 1</h3>
                {{--within the p tag, question 1 is pulled from the database using question_1 --}}
                    <p>{{$questions->question_1}}</p>
                <ul>
                    {{--the below section of code is used to created multiple choice answers for the questions using radio buttons,
                    name="answers_1" pulls the data from the database for answer 1 with its three possible answers usings answers 1, 2 and 3--}}
                    List of answers:
                    <li><input type="radio" name="answers_1"
                           <?php if (isset($answer_1) && $answer_1==$answer_1) echo "checked";?>
                           value={!! $questions->answer_1 !!}>{!! $questions->answer_1 !!}</li>
                    <li><input type="radio" name="answers_1"
                           <?php if (isset($answer_2) && $answer_2==$answer_2) echo "checked";?>
                           value={!! $questions->answer_2 !!}>{!! $questions->answer_2 !!}</li>
                    <li><input type="radio" name="answers_1"
                               <?php if (isset($answer_3) && $answer_3==$answer_3) echo "checked";?>
                               value={!! $questions->answer_3 !!}>{!! $questions->answer_3 !!}</li>
                    </ul>
                    {{--the below section of code defines the variables for the possible answers --}}
                    <?php
                    $answer = "";
                    ?>
                    <br><br>
                    <?php
                    echo $answer;
                    ?>

                    {{--Question 2--}}

                    <h3>Question 2</h3>
                    <p>{{$questions->question_2}}</p>
                    <ul>
                        {{--the below section of code is used to created multiple choice answers for the questions using radio buttons,
                    name="answers_1" pulls the data from the database for answer 3 with its three possible answers usings answers 4, 5 and 6--}}
                        List of answers:
                        <li><input type="radio" name="answers_2"
                                   <?php if (isset($answer_4) && $answer_4==$answer_4) echo "checked";?>
                                   value={!! $questions->answer_4!!}>{!! $questions->answer_4 !!}</li>
                        <li><input type="radio" name="answers_2"
                                   <?php if (isset($answer_5) && $answer_5==$answer_5) echo "checked";?>
                                   value={!! $questions->answer_5 !!}>{!! $questions->answer_5 !!}</li>
                        <li><input type="radio" name="answers_2"
                                   <?php if (isset($answer_6) && $answer_6==$answer_6) echo "checked";?>
                                   value={!! $questions->answer_6 !!}>{!! $questions->answer_6 !!}</li>
                    </ul>
                    {{--the below section of code defines the variables for the possible answers --}}
                    <?php
                    $answer = "";
                    ?>
                    <br><br>
                    <?php
                    echo $answer;
                    ?>

                    {{--Question 3--}}

                    <h3>Question 3</h3>
                    <p>{{$questions->question_3}}</p>
                    <ul>
                        {{--the below section of code is used to created multiple choice answers for the questions using radio buttons,
                    name="answers_1" pulls the data from the database for answer 3 with its three possible answers usings answers 7, 8 and 9--}}
                        List of answers:
                        <li><input type="radio" name="answers_3"
                                   <?php if (isset($answer_7) && $answer_7==$answer_7) echo "checked";?>
                                   value={!! $questions->answer_7!!}>{!! $questions->answer_7 !!}</li>
                        <li><input type="radio" name="answers_3"
                                   <?php if (isset($answer_8) && $answer_8==$answer_8) echo "checked";?>
                                   value={!! $questions->answer_8 !!}>{!! $questions->answer_8 !!}</li>
                        <li><input type="radio" name="answers_3"
                                   <?php if (isset($answer_9) && $answer_9==$answer_9) echo "checked";?>
                                   value={!! $questions->answer_9 !!}>{!! $questions->answer_9 !!}</li>
                    </ul>
                    {{--the below section of code defines the variables for the possible answers --}}
                    <?php
                    $answer = "";
                    ?>
                    <br><br>
                    <?php
                    echo $answer;
                    ?>
                    {{--Question 4--}}

                    <h3>Question 4</h3>
                    <p>{{$questions->question_4}}</p>
                    <ul>
                        {{--the below section of code is used to created multiple choice answers for the questions using radio buttons,
                    name="answers_1" pulls the data from the database for answer 4 with its three possible answers usings answers 10, 11 and 12--}}
                        List of answers:
                        <li><input type="radio" name="answers_4"
                                   <?php if (isset($answer_10) && $answer_10==$answer_10) echo "checked";?>
                                   value={!! $questions->answer_10!!}>{!! $questions->answer_10 !!}</li>
                        <li><input type="radio" name="answers_4"
                                   <?php if (isset($answer_11) && $answer_11==$answer_11) echo "checked";?>
                                   value={!! $questions->answer_11 !!}>{!! $questions->answer_11 !!}</li>
                        <li><input type="radio" name="answers_4"
                                   <?php if (isset($answer_12) && $answer_12==$answer_12) echo "checked";?>
                                   value={!! $questions->answer_12 !!}>{!! $questions->answer_12 !!}</li>
                    </ul>
                    {{--the below section of code defines the variables for the possible answers --}}
                    <?php
                    $answer = "";
                    ?>
                    <br><br>
                    <?php
                    echo $answer;
                    ?>
                    {{--Question 5--}}

                    <h3>Question 5</h3>
                    <p>{{$questions->question_5}}</p>
                    <ul>
                        {{--the below section of code is used to created multiple choice answers for the questions using radio buttons,
                    name="answers_1" pulls the data from the database for answer 5 with its three possible answers usings answers 13, 14 and 15--}}
                        List of answers:
                        <li><input type="radio" name="answers_5"
                                   <?php if (isset($answer_13) && $answer_13==$answer_13) echo "checked";?>
                                   value={!! $questions->answer_13!!}>{!! $questions->answer_13 !!}</li>
                        <li><input type="radio" name="answers_5"
                                   <?php if (isset($answer_14) && $answer_14==$answer_14) echo "checked";?>
                                   value={!! $questions->answer_14 !!}>{!! $questions->answer_14 !!}</li>
                        <li><input type="radio" name="answers_5"
                                   <?php if (isset($answer_15) && $answer_15==$answer_15) echo "checked";?>
                                   value={!! $questions->answer_15 !!}>{!! $questions->answer_15 !!}</li>
                        <input type="hidden" name="questionnaire_id" value="{{ $questionnaire->id }}">
                    </ul>
                    {{--the below section of code defines the variables for the possible answers --}}
                    <?php
                    $answer = "";
                    ?>
                    {{--here a form is opened, within the form a button is created to add the questtionnaire, the form is
                     then closed--}}
                    <div class="row large-4 columns">
                        {!! Form::submit('Add Questionnaire', ['class' => 'button']) !!}
                    </div>
                    {!! Form::close() !!}
                    <br><br>
                    <?php
                    echo $answer;
                    ?>
                @endforeach
                {{--here shows the creator of the questionnaire, this works by pulling the user name from the database--}}
                <p>creator: {{ $questionnaire->user->name }}</p>
                <div class="row">
                </div>
            </div>
    </div>
    </div>
@endsection










