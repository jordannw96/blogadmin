{{--here the layout is used, this is taken from the layouts folder which contains the layout for all views--}}
@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Questionnaires</div>

                    <div class="panel-body">


                        <section>

                            @if (isset ($questionnaires))
                                {{--here a table is created, a class used to make the table striped, three table headings are created using the td tags--}}
                                <table class="table-striped table-bordered" style="width: 100%">
                                    <thead>
                                    <tr>
                                        <td>Title</td>
                                        <td>Details</td>
                                        <td>View Results</td>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    {{--here a loop is created for showing the questionnairs to be displayed using @foreach --}}
                                    @foreach($questionnaires as $questionnaire)
                                    <tr>
                                        {{--here is the content underneath the three headings created in the table, the first line is a link to the questionnaires
                                         which have been created, when clicked it brings up the questionnaire with the questions to put in. The second line shows whatever is
                                         enetered into the content section within the creation of the questionnaire. The third line is a link to the results view, this
                                         works by pulling through the questionnaire id and title from the database--}}
                                        <td><a href="/admin/questionnaire/{{  $questionnaire->id }}" name="{{ $questionnaire->title }}">{{$questionnaire->title }}</a></td>
                                        <td>{{$questionnaire->content}}</td>
                                        <td><a href="/admin/results/{{  $questionnaire->id }}" name="{{ $questionnaire->title }}">View Results</a></td>
                                    {{--here the loop ends, using @endforeach--}}
                                    @endforeach
                                    </tbody>
                                </table>
                                @else
                            @endif
                        </section>
                        {{--here a form is opened which links it to the questionnaire controller, once the button is clicked it will add the questionnaire
                        to the database--}}
                        {{ Form::open(array('action' => 'QuestionnaireController@create', 'method' => 'get')) }}
                        <div class="row">
                            {!! Form::submit('Add Questionnaire', ['class' => 'button']) !!}
                        </div>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
    </div>
    </div>
            @endsection






