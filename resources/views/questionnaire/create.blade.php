@extends('layouts.app')

@section('content')
    <div class="col-md-8 col-md-offset-2">
        <div class="panel panel-default">
            <div class="panel-body">

    <title>Questionnaire</title>



<h1>Add Questionnaire</h1>

{{--This is the start of the form, an array is openned which links the form to the Questionnaire controller, using the id and createQuestionnaire view --}}
{!! Form::open(array('action' => 'QuestionnaireController@store', 'id' => 'createQuestionnaire')) !!}
{{ csrf_field() }}
<div class="row large-12 columns">
    {{--When the form is opened, the user is given an option to eneter a title and ethics for the questionnaire, this bit of the code does that--}}
    {!! Form::label('title', ' Title:  ') !!}
    {!! Form::text('title', null, ['class' => 'large-8 columns']) !!}
</div>

<div class="row large-12 columns">
    {!! Form::label('content', 'Ethics:') !!}
    {!! Form::textarea('content', null, ['class' => 'large-8 columns']) !!}

</div>

<div class="row large-4 columns">
    {{--Here is a button which submits the form--}}
    {!! Form::submit('Add Questionnaire', ['class' => 'button']) !!}
</div>
{!! Form::close() !!}

</div>
        </div>
    </div>
@endsection