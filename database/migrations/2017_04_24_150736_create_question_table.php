<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuestionTable extends Migration
{
    /**
    here is where the tables are created which are migrated to the database
     */
    public
    function up()
    {
        Schema::create('questions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('question_1');
            $table->string('question_2');
            $table->string('question_3');
            $table->string('question_4');
            $table->string('question_5');
            $table->integer('questionnaire_id')->unsigned();
            $table->integer('author_id')->unsigned()->default(0);
            $table->foreign('questionnaire_id')->references('id')->on('questionnaires');
            $table->string('answer_1');
            $table->string('answer_2');
            $table->string('answer_3');
            $table->string('answer_4');
            $table->string('answer_5');
            $table->string('answer_6');
            $table->string('answer_7');
            $table->string('answer_8');
            $table->string('answer_9');
            $table->string('answer_10');
            $table->string('answer_11');
            $table->string('answer_12');
            $table->string('answer_13');
            $table->string('answer_14');
            $table->string('answer_15');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public
    function down()
    {
        Schema::drop('questions');

    }
}
