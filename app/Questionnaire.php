<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Questionnaire extends Model
{
    // this sends all of the data within the fillables to send over using one field
    protected $fillable = [
        'title',
        'content',
        'question'
    ];



    public function user()
    {
        return $this->belongsTo('App\User', 'author_id');
    }
    public function question()
    {
        return $this->hasMany('App\Question', 'author_id');
    }
}
