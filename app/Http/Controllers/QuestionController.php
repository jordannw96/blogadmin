<?php

namespace App\Http\Controllers;
// here it is using the different models to access the database
use App\Question;
use App\Questionnaire;
use Illuminate\Http\Request;

use App\Http\Requests;


use Auth;

class QuestionController extends Controller
{
    /*
   Here a public function is created for authorisation, to make sure the user is logged in
   */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /*
     * here the function index is created */
    public function index()
    {
        // gets all the questions
        $questions = Question::all();
// this returns to the index view, pulling the questions through
        return view('questionnaire.index', ['questions' => $questions]);
    }

    /**
   here an id is created, find or fail is used to look for the questionnaire, it either finds it or fails,
     * then it returns to the create questions view, pulling through the questionnaire
     */
    public function create($id)
    {
        $questionnaire = Questionnaire::findOrFail($id);

        // now we can return the data with the view
        return view('questions.create', ['questionnaire' => $questionnaire]);

    }

    /**
    here a public function is created to store data in the database, it stores the questions and the author id and saves it
     * it then returns to the questionnaire view
     */
    public function store(Request $request)
    {
      $questions = Question::create($request->all());
        $questions->author_id=\Illuminate\Support\Facades\Auth::user()->id;
        $questions->save();
        return redirect('/admin/questionnaire/');
    }
    /**
    here a public function is created to actually show the data on the view using the id of the questions,
     * after the user has submitted the questions it returns the show questions view
     */
    public function show($id)
    {

        // get the article
        $questions = question::with('user')->where('id', $id)->first();

        // if article does not exist return to list
        if(!$questions)
        {
            return redirect('/admin/questions'); // you could add on here the flash messaging of article does not exist.
        }
        return view('questions.show')->withQuestion($questions);

    }

    public function edit($id)
    {
        //
    }


    public function update(Request $request, $id)
    {
        //
    }


    public function destroy($id)
    {
        //
    }
}
