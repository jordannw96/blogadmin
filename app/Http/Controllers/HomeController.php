<?php

namespace App\Http\Controllers;
// here it is using the different models to access the database
use App\Http\Requests;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
    Here a public function is created for authorisation, to make sure the user is logged in
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     here a public function is created, when it is run it returns to the home view
     */
    public function index()
    {
        return view('home');
    }
}
