<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
// here it is using the different models to access the database
use App\Http\Requests;
use App\Questionnaire;
use App\Question;
use App\Answer;

use Auth;

class QuestionnaireController extends Controller
{
    /*
   Here a public function is created for authorisation, to make sure the user is logged in
   */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        // gets all the questionnaires and questions and returns the index view
        $questionnaires = Questionnaire::all();
        $questions = Question::all();
        return view('questionnaire.index', ['questionnaires' => $questionnaires, 'question' => $questions]);
    }

    /**
     * here the function is created to create the questionnaire and returns the create questionnaire view
     */
    public function create()
    {


        // now we can return the data with the view
        return view('questionnaire.create');

    }

    /**
     * here a public function is created to store data in the database, it stores the questionnaire and the author id and saves it
     * it then returns to the create questionnaire view
     */
    public function store(Request $request)
    {
        $questionnaire = Questionnaire::create($request->all());
        $questionnaire->author_id = \Illuminate\Support\Facades\Auth::user()->id;
        $questionnaire->save();


        return redirect('/admin/question/create/' . $questionnaire->id);
    }

    /**
     * here a public function is created to actually show the data on the view using the id of the questionnaires,
     * after the user has submitted the questionnaire it returns the show questionnaires view
     */
    public function show($id)
    {

        // get the article
        $questionnaire = Questionnaire::where('id', $id)->first();
        $question = Question::where('questionnaire_id', '=', $id)->get();
        // if article does not exist return to list
        if (!$questionnaire) {
            return redirect('/admin/questionnaire'); // you could add on here the flash messaging of article does not exist.
        }
        return view('questionnaire/show')->withQuestionnaire($questionnaire)->withQuestion($question);

//        return view('questionnaire/show', ['questionnaire'=>$questionnaire, 'question'=>$question]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }


    public function update(Request $request, $id)
    {
        //
    }


    public function destroy($id)
    {

    }
}