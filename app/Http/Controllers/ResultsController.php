<?php

namespace App\Http\Controllers;
// here it is using the different models to access the database
use
    Illuminate\Http\Request;
use App\Results;
use App\Questionnaire;
use App\Http\Requests;

class ResultsController extends Controller
{
    /**
    Here a public function is created for authorisation, to make sure the user is logged in
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
// here a function is created to return the show results view
    public function index()
    {


        return view('results.show');
    }

/*
 *here a public function is created to store data in the database, it stores the results and the author id and saves it
     * it then returns to the questionnaire view*/
    public function store(Request $request)
    {
        $questionnaire = Results::create($request->all());
//        $questionnaire->categories()->attach($request->input('category'));
//        $questionnaire->author_id = Auth::user()->id;
        $questionnaire->save();

        return redirect('/admin/questionnaire');
    }


    /**
     * here a public function is created to actually show the data from the results on the view using the id of the questionnaires,
     * after the user has submitted the questionnaire it returns the show results view
     */
    public function show($id)
    {
        $questionnaire = Questionnaire::findOrFail($id);
        $results = Results::where('questionnaire_id', $id)->get();
        return view('results.show', ['questionnaire' => $questionnaire, 'results' => $results]);
    }


    public function edit($id)
    {
        //
    }


    public function update(Request $request, $id)
    {
        //
    }


    public function destroy($id)
    {
        //
    }
}