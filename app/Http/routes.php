<?php

Route::get('/', function () {
    return view('welcome');
});

Route::group(['prefix' => 'api/v1'], function(){
    Route::resource('articles', 'OpenArticleController');
});

Route::group(['middleware' => 'web'], function () {
    Route::auth();

    Route::get('/home', 'HomeController@index');
    Route::resource('/admin/articles', 'ArticleController' );
    Route::resource('/admin/users', 'UserController' );
    Route::resource('/admin/questionnaire', 'QuestionnaireController');
    Route::resource('/admin/question/create', 'QuestionController@create');
    Route::resource('/admin/questionnaires', 'QuestionnaireController');
    Route::resource('/admin/questions', 'QuestionController');
    Route::resource('/admin/results', 'ResultsController');
    Route::resource('/admin/results/show', 'ResultsController');


});
