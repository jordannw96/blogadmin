<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    // this sends all of the data within the fillables to send over using one field
    protected $fillable = [
        'question_1',
        'question_2',
        'question_3',
        'question_4',
        'question_5',
        'questionnaire_id',
        'answer_1',
        'answer_2',
        'answer_3',
        'answer_4',
        'answer_5',
        'answer_6',
        'answer_7',
        'answer_8',
        'answer_9',
        'answer_10',
        'answer_11',
        'answer_12',
        'answer_13',
        'answer_14',
        'answer_15',

    ];


    public function user()
    {
        return $this->belongsTo('App\Questionnaire', 'author_id');
    }
}
