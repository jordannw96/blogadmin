<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    // this sends all of the data within the fillables to send over using one field
    protected $fillable = [
        'answers_title',
        'question_id'

    ];


    public function categories()
    {
        return $this->belongsToMany('App\Category');
    }


    public function user()
    {
        return $this->belongsTo('App\User', 'author_id');
    }
}
