<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Results extends Model
{
    // this sends all of the data within the fillables to send over using one field
    protected $fillable = [
        'title',
        'questionnaire_id',
        'content',
        'answers_1',
        'answers_2',
        'answers_3',
        'answers_4',
        'answers_5',


    ];

    public function questionnaires()
    {
        return $this->belongsTo('App\Questionnaire');
    }
}
